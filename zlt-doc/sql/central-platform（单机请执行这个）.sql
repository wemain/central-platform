/*
Navicat MySQL Data Transfer

Source Server         : local-mysql-ts
Source Server Version : 50713
Source Host           : 127.0.0.1:3306
Source Database       : central-platform

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2019-09-23 11:43:51
*/
CREATE DATABASE IF NOT EXISTS `central-platform` DEFAULT CHARACTER SET = utf8;
Use `central-platform`;


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for file_info
-- ----------------------------
DROP TABLE IF EXISTS `file_info`;
CREATE TABLE `file_info` (
  `id` varchar(32) NOT NULL COMMENT '文件md5',
  `name` varchar(128) NOT NULL,
  `is_img` tinyint(1) NOT NULL,
  `content_type` varchar(128) NOT NULL,
  `size` int(11) NOT NULL,
  `path` varchar(255) DEFAULT NULL COMMENT '物理路径',
  `url` varchar(1024) NOT NULL,
  `source` varchar(32) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`),
  KEY `idx_create_time` (`create_time`),
  KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `client_id` varchar(32) NOT NULL COMMENT '应用标识',
  `resource_ids` varchar(256) DEFAULT NULL COMMENT '资源限定串(逗号分割)',
  `client_secret` varchar(256) DEFAULT NULL COMMENT '应用密钥(bcyt) 加密',
  `client_secret_str` varchar(256) DEFAULT NULL COMMENT '应用密钥(明文)',
  `scope` varchar(256) DEFAULT NULL COMMENT '范围',
  `authorized_grant_types` varchar(256) DEFAULT NULL COMMENT '5种oauth授权方式(authorization_code,password,refresh_token,client_credentials)',
  `web_server_redirect_uri` varchar(256) DEFAULT NULL COMMENT '回调地址 ',
  `authorities` varchar(256) DEFAULT NULL COMMENT '权限',
  `access_token_validity` int(11) DEFAULT NULL COMMENT 'access_token有效期',
  `refresh_token_validity` int(11) DEFAULT NULL COMMENT 'refresh_token有效期',
  `additional_information` varchar(4096) DEFAULT '{}' COMMENT '{}',
  `autoapprove` char(4) NOT NULL DEFAULT 'true' COMMENT '是否自动授权 是-true',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `client_name` varchar(128) DEFAULT '' COMMENT '应用名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('1', 'webApp', null, '$2a$10$06msMGYRH8nrm4iVnKFNKOoddB8wOwymVhbUzw/d3ZixD7Nq8ot72', 'webApp', 'app', 'authorization_code,password,refresh_token,client_credentials', null, null, '3600', null, '{}', 'true', null, null, 'pc端');
INSERT INTO `oauth_client_details` VALUES ('2', 'app', null, '$2a$10$i3F515wEDiB4Gvj9ym9Prui0dasRttEUQ9ink4Wpgb4zEDCAlV8zO', 'app', 'app', 'password,refresh_token', null, null, '3600', null, '{}', 'true', null, null, '移动端');
INSERT INTO `oauth_client_details` VALUES ('3', 'zlt', null, '$2a$10$/o.wuORzVcXaezmYVzwYMuoY7qeWXBALwQmkskXD/7C6rqfCyPrna', 'zlt', 'all', 'authorization_code,password,refresh_token,client_credentials', 'http://127.0.0.1:8080/singleLogin', null, '3600', '28800', '{}', 'true', '2018-12-27 00:50:30', '2018-12-27 00:50:30', '第三方应用');

-- ----------------------------
-- Table structure for sys_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionary`;
CREATE TABLE `sys_dictionary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dict_type` varchar(50) NOT NULL,
  `dict_code` varchar(63) NOT NULL,
  `dict_name_cn` varchar(200) NOT NULL,
  `dict_name_en` varchar(200) NOT NULL,
  `sort` int(11) NOT NULL,
  `parent_dict_type` varchar(255) DEFAULT NULL,
  `create_operator` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_operator` varchar(50) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=645 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dictionary
-- ----------------------------
INSERT INTO `sys_dictionary` VALUES ('1', 'Language', 'en_US', '英文', 'English', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('2', 'Language', 'zh_CN', '中文', 'Chinese', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('3', 'Menu_Type', '0', '菜单', 'Menu', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('4', 'Menu_Type', '1', '功能', 'Function', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('5', 'Sex_Type', 'female', '女', 'Female', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('6', 'Sex_Type', 'male', '男', 'Male', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('7', 'TrueorFalse_type', '0', '否', 'false', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('8', 'TrueorFalse_type', '1', '是', 'true', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('9', 'Validity_Type', 'invalid', '无效', 'invalid', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('10', 'Validity_Type', 'valid', '有效', 'valid', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('11', 'User_Type', '0', '系统用户', 'System user', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('12', 'User_Type', '02', '内部库审员', 'Inner Auditor', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('13', 'User_Type', '03', '外包公司', 'Vendor', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('14', 'User_Type', '3', '外包库审员', 'Vendor Auditor', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('15', 'User_Type', '4', '经销商', 'Dealer', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('16', 'holiday_flag', '0', '节假日', 'Holiday', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('17', 'holiday_flag', '1', '特殊工作日', 'Workday', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('18', 'partnerType', 'partnerType:1', '企业', '', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('19', 'authenticatedType', 'authenticatedType:1', '线上自助', '', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('20', 'authenticatedType', 'authenticatedType:2', '线下维护', '', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('21', 'EnterpriseCertificateNo', 'EnterpriseCertificateNo:1', '社会统一信用代码', '', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('22', 'EnterpriseCertificateNo', 'EnterpriseCertificateNo:2', '组织机构', '', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('23', 'authenticatedState', 'authenticatedState:1', '未认证', '', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('24', 'authenticatedState', 'authenticatedState:2', '待审核', '', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('25', 'authenticatedState', 'authenticatedState:3', '已认证', '', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('26', 'authenticatedState', 'authenticatedState:4', '不通过', '', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('27', 'partnerType', 'partnerType:2', '个人', '', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('28', 'customerGender', 'SYS_LIBRARY_GENDER:0', '未知性别', 'ss', '0', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('29', 'customerGender', 'SYS_LIBRARY_GENDER:1', '男', 'nan', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('30', 'customerGender', 'SYS_LIBRARY_GENDER:2', '女', 'nv', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('31', 'companyId', '77', '同方全球人寿保险有限公司', 'a', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('32', 'companyId', '78', '瑞泰人寿保险有限公司', '1', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('33', 'companyId', '79', '中德安联人寿保险有限公司', '3', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('34', 'companyId', '81', '阳光人寿保险股份有限公司', '4', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('35', 'productType2', 'PDT_PRODUCT_TYPE2:1', '(寿)普通寿险', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('36', 'productType2', 'PDT_PRODUCT_TYPE2:2', '(寿)分红寿险', '2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('37', 'productType2', 'PDT_PRODUCT_TYPE2:3', '(寿)投连险', '3', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('38', 'productType2', 'PDT_PRODUCT_TYPE2:4', '(寿)万能寿险', '4', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('39', 'productType2', 'PDT_PRODUCT_TYPE2:5', '(寿)意外险', '5', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('40', 'productType3', 'PDT_PRODUCT_TYPE3:12', '投资型家财险', '12', '12', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('41', 'productType3', 'PDT_PRODUCT_TYPE3:13', '交强险', '13', '13', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('42', 'productType3', 'PDT_PRODUCT_TYPE3:14', '机动车辆消费贷款保证保险', '14', '14', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('43', 'productType3', 'PDT_PRODUCT_TYPE3:15', '个人贷款抵押房屋保证保险', '15', '15', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('44', 'productType3', 'PDT_PRODUCT_TYPE3:16', '投资型健康险', '16', '16', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('45', 'productType4', 'PDT_PRODUCT_TYPE4:1', '短期意外', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('46', 'productType4', 'PDT_PRODUCT_TYPE4:11', '无', '11', '11', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('47', 'productType4', 'PDT_PRODUCT_TYPE4:2', '风险保障', '2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('48', 'productType4', 'PDT_PRODUCT_TYPE4:3', '大病保障', '3', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('49', 'productType4', 'PDT_PRODUCT_TYPE4:4', '医疗报销', '4', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('50', 'productType5', 'PDT_PRODUCT_TYPE5:0', '年金', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('51', 'productType5', 'PDT_PRODUCT_TYPE5:1', '终寿定寿', '2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('52', 'productType5', 'PDT_PRODUCT_TYPE5:2', '重疾', '3', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('53', 'productType5', 'PDT_PRODUCT_TYPE5:3', '医疗', '4', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('54', 'productType5', 'PDT_PRODUCT_TYPE5:4', '意外', '5', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('55', 'productType5', 'PDT_PRODUCT_TYPE5:5', '理财', '6', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('56', 'productType6', 'PDT_PRODUCT_TYPE6:0', '消费型', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('57', 'productType6', 'PDT_PRODUCT_TYPE6:1', '返回型', '2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('58', 'insType', '1', '主险', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('59', 'insType', '0', '附加险', '2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('60', 'isValid', '1', '有效', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('61', 'isValid', '0', '失效', '2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('62', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:11', '身份证', '身份证', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('63', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:12', '户口薄', '户口薄', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('64', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:13', '驾驶证', '驾驶证', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('65', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:14', '军官证', '军官证', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('66', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:15', '士兵证', '士兵证', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('67', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:16', '军官离退休证', '军官离退休证', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('68', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:17', '中国护照', '中国护照', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('69', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:21', '外国护照', '外国护照', '21', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('70', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:22', '旅行证', '旅行证', '22', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('71', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:23', '回乡证', '回乡证', '23', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('72', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:24', '居留证件', '居留证件', '24', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('73', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:29', '其他个人证件', '其他个人证件', '29', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('74', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:31', '组织机构代码证', '组织机构代码证', '31', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('75', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:32', '工商登记证', '工商登记证', '32', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('76', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:33', '税务登记证', '税务登记证', '33', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('77', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:34', '营业执照', '营业执照', '34', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('78', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:55', '护照', '护照', '55', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('79', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:56', '港澳通行证', '港澳通行证', '56', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('80', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:57', '台胞证', '台胞证', '57', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('81', 'mainIdType', 'SYS_LIBRARY_CERTI_TYPE:99', '其他证件', '其他证件', '99', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('82', 'isSuccessName', 'CBS_CONTRACTLIFE_STATUS:1', '承保', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('83', 'parentCompanyId', 'o', '安邦', 'ab', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('84', 'CustomerIdentity', 'CRM_CUSTOMER_TYPE:1', '投保人', 'insur', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('85', 'CustomerIdentity', 'CRM_CUSTOMER_TYPE:2', '被保人', '被保人', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('86', 'CustomerIdentity', 'CRM_CUSTOMER_TYPE:3', '受益人', '受益人', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('87', 'CustomerIdentity', 'CRM_CUSTOMER_TYPE:4', '其他', '其他', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('88', 'jobType', 'SYS_LIBRARY_JOBTYPE:1', '专业技术人员', '专业技术人员', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('89', 'CBS_POLICY_TYPE', 'CBS_POLICY_TYPE:0', '个险', '个险', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('90', 'CBS_POLICY_TYPE', 'CBS_POLICY_TYPE:1', '团险', '团险', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('91', 'CBS_POLICY_TYPE', 'CBS_POLICY_TYPE:2', '财险', '财险', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('92', 'CBS_POLICY_TYPE', 'CBS_POLICY_TYPE:3', '车险', '车险', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('93', 'CBS_POLICY_BUSINESS_TYPE', 'CBS_POLICY_BUSINESS_TYPE:1', '网销', '网销', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('94', 'CBS_POLICY_BUSINESS_TYPE', 'CBS_POLICY_BUSINESS_TYPE:2', '非网销', '非网销', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('95', 'CBS_POLICY_BUSINESS_TYPE', 'CBS_POLICY_BUSINESS_TYPE:3', '平台件', '平台件', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('96', 'preservationType', 'POLICY_PRESERV_TYPE:7', '保单状态变更', '保单状态变更', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('97', 'CBS_POLICY_STATUS', 'CBS_POLICY_STATUS:0', '预收', '预收', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('98', 'CBS_POLICY_STATUS', 'CBS_POLICY_STATUS:1', '承保', '承保', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('99', 'CBS_POLICY_STATUS', 'CBS_POLICY_STATUS:2', '撤单', '撤单', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('100', 'CBS_POLICY_STATUS', 'CBS_POLICY_STATUS:3', '终止件', '终止件', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('101', 'CBS_POLICY_STATUS', 'CBS_POLICY_STATUS:4', '契撤件', '契撤件', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('102', 'CBS_POLICY_STATUS', 'CBS_POLICY_STATUS:5', '拒保', '拒保', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('103', 'CBS_POLICY_STATUS', 'CBS_POLICY_STATUS:6', '延期承保', '延期承保', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('104', 'preservationCode', 'POLICY_PRESERV_CODE:16', '退保', '退保', '16', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('105', 'preservationCode', 'POLICY_PRESERV_CODE:17', '犹豫期退保', '犹豫期退保', '17', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('106', 'qualification', 'SYS_LIBRARY_DEGREE_TYPE:0', '无', 'u', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('107', 'qualification', 'SYS_LIBRARY_DEGREE_TYPE:1', '博士', 's', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('108', 'qualification', 'SYS_LIBRARY_DEGREE_TYPE:3', '硕士', 'a', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('109', 'qualification', 'SYS_LIBRARY_DEGREE_TYPE:', '学士', '1', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('110', 'nationality', 'SYS_LIBRARY_NATIONALITY:CN', '中国', 'cn', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('111', 'political', 'SYS_LIBRARY_POLITICAL:1', '中共党员', '中共党员', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('112', 'political', 'SYS_LIBRARY_POLITICAL:2', '预备党员', '预备党员', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('113', 'political', 'SYS_LIBRARY_POLITICAL:3', '共青团员', '共青团员', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('114', 'political', 'SYS_LIBRARY_POLITICAL:13', '群众', '群众', '13', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('115', 'political', 'SYS_LIBRARY_POLITICAL:12', '无党派人士', '无党派人士', '12', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('116', 'educated', 'SYS_LIBRARY_EDUCATION:1', '文盲', '文盲', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('117', 'educated', 'SYS_LIBRARY_EDUCATION:2', '小学', '小学', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('118', 'educated', 'SYS_LIBRARY_EDUCATION:3', '初中', '初中', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('119', 'educated', 'SYS_LIBRARY_EDUCATION:4', '高中', '高中', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('120', 'educated', 'SYS_LIBRARY_EDUCATION:5', '中专', '中专', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('121', 'educated', 'SYS_LIBRARY_EDUCATION:6', '中技', '中技', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('122', 'SYS_FLAG', 'SYS_FLAG:0', '否', 'no', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('123', 'educated', 'SYS_LIBRARY_EDUCATION:7', '大专', '大专', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('124', 'educated', 'SYS_LIBRARY_EDUCATION:8', '本科', '本科', '8', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('125', 'SYS_PAY_CHANNEL', 'wxpay', '微信支付', '微信支付', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('126', 'educated', 'SYS_LIBRARY_EDUCATION:9', '研究生及以上', '研究生及以上', '9', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('127', 'SYS_FLAG', 'SYS_FLAG:1', '是', 'yes', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('128', 'SYS_PAY_CHANNEL', 'alipay', '支付宝', '支付宝', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('129', 'educated', 'SYS_LIBRARY_EDUCATION:10', '博士', '博士', '10', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('130', 'educated', 'SYS_LIBRARY_EDUCATION:11', '博士后', '博士后', '11', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('131', 'ODR_PAY_STATUS', '0', '支付成功', '支付成功', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('132', 'ODR_PAY_STATUS', '1', '支付中', '支付中', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('133', 'ODR_PAY_STATUS', '2', '已支付', '已支付', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('134', 'ODR_PAY_STATUS', '3', '支付失败', '支付失败', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('135', 'maritalState', 'SYS_LIBRARY_MARITAL:10', '未婚', '未婚', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('136', 'maritalState', 'SYS_LIBRARY_MARITAL:20', '已婚', '已婚', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('137', 'maritalState', 'SYS_LIBRARY_MARITAL:90', '未说明的婚姻状况', '未说明的婚姻状况', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('138', 'maritalState', 'SYS_LIBRARY_MARITAL:40', '离婚', '离婚', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('139', 'healthState', 'SYS_LIBRARY_HEALTH:1', '健康或良好', '健康或良好', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('140', 'healthState', 'SYS_LIBRARY_HEALTH:2', '一般或较弱', '一般或较弱', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('141', 'healthState', 'SYS_LIBRARY_HEALTH:3', '有慢性病', '有慢性病', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('142', 'healthState', 'SYS_LIBRARY_HEALTH:6', '残疾', '残疾', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('143', 'problemType', 'CBS_PROBLEM_TYPE:1', '业务问题件', 'problem', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('144', 'problemType', 'CBS_PROBLEM_TYPE:2', '财务问题件', 'problem', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('145', 'originType', 'CBS_PROBLEM_ORIGIN_TYPE:1', '新契约受理', 'a', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('146', 'originType', 'CBS_PROBLEM_ORIGIN_TYPE:2', '保全', 'b', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('147', 'originType', 'CBS_PROBLEM_ORIGIN_TYPE:3', '理赔', 'c', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('148', 'problemStatus', 'CBS_PROBLEM_STATUS:1', '待处理', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('149', 'problemStatus', 'CBS_PROBLEM_STATUS:2', '已处理', '2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('150', 'customerType', 'CBS_INSURED_TYPE:0', '自然人', '自然人', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('151', 'customerType', 'CBS_INSURED_TYPE:1', '法人', '法人', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('152', 'COMPANY_COMMISSION_TYPE', 'COMPANY_COMMISSION_TYPE:1', '首续年佣金', '首续年佣金', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('153', 'SET_STATUS_CODE', 'SET_STATUS_CODE:0', '对账成功', '对账成功', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('154', 'SET_STATUS_CODE', 'SET_STATUS_CODE:1', '有差异', '有差异', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('155', 'SET_STATUS_CODE', 'SET_STATUS_CODE:2', '未对账', '未对账', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('156', 'SET_STATUS_CODE', 'SET_STATUS_CODE:3', '对账中', '对账中', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('157', 'SET_STATUS_CODE', 'SET_STATUS_CODE:4', '差异已处理', '差异已处理', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('158', 'periodType', 'PDT_PRODUCT_PERIODTYPE:0', '无关或不确定', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('159', 'periodType', 'PDT_PRODUCT_PERIODTYPE:1', '极短期', '2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('160', 'periodType', 'PDT_PRODUCT_PERIODTYPE:2', '短期', '3', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('161', 'periodType', 'PDT_PRODUCT_PERIODTYPE:3', '长期', '4', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('162', 'SET_REC_WAY', 'SET_REC_WAY:1', '税前对账', '税前对账', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('163', 'SET_REC_WAY', 'SET_REC_WAY:2', '税后对账', '税后对账', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('164', 'jobPosition', 'CRM_POSITION_TYPE:0', '销售管理人员', '销售管理人员', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('165', 'jobPosition', 'CRM_POSITION_TYPE:1', '服务支持', '服务支持', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('166', 'jobPosition', 'CRM_POSITION_TYPE:2', '销售人员', '销售人员', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('167', 'inJobStatus', 'ON_JOB_STATUS:1', '在职', '在职', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('168', 'inJobStatus', 'ON_JOB_STATUS:2', '离职', '离职', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('169', 'CBS_BUSI_TYPE', 'CBS_BUSI_TYPE:0', '新契约', '新契约', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('170', 'CBS_BUSI_TYPE', 'CBS_BUSI_TYPE:1', '犹豫期退保', '犹豫期退保', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('171', 'CBS_BUSI_TYPE', 'CBS_BUSI_TYPE:2', '退保', '退保', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('172', 'CBS_BUSI_TYPE', 'CBS_BUSI_TYPE:7', '续期', '续期', '8', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('173', 'ODR_OPT_ENV', 'ODR_OPT_ENV:ONLINE', '线上业务', '线上业务', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('174', 'ODR_OPT_ENV', 'ODR_OPT_ENV:OFFLINE', '线下业务', '线下业务', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('175', 'GENDER', '1', '男', 'Man', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('176', 'GENDER', '2', '女', 'Women', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('177', 'GENDER', '0', '未知性别', 'Unknown', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('178', 'contractType', 'PDT_DEAL_TYPE:0', '主协议', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('179', 'contractType', 'PDT_DEAL_TYPE:1', '补充协议', '2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('180', 'contractType2', 'PDT_DEAL_CATEGORY:1', '产品&手续费协议', '2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('181', 'signMethod', 'SIGN_MODE:0', '总对总', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('182', 'premCollectMethod', 'PREM_SETTLE_MODE:0', '公司划账', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('183', 'premCollectMethod', 'PREM_SETTLE_MODE:1', '转账日结', '2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('184', 'premCollectMethod', 'PREM_SETTLE_MODE:2', '转账月结', '3', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('185', 'premCollectMethod', 'PREM_SETTLE_MODE:3', '划账&转账', '4', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('186', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:1', '承保', '承保', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('187', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:2', '拒保', '拒保', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('188', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:3', '犹豫期退保', '犹豫期退保', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('189', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:4', '退保', '退保', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('190', 'SYS_LIBRARY_GENDER', 'SYS_LIBRARY_GENDER:0', '未知的性别', '未知的性别', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('191', 'SYS_LIBRARY_GENDER', 'SYS_LIBRARY_GENDER:1', '男', '男', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('192', 'SYS_LIBRARY_GENDER', 'SYS_LIBRARY_GENDER:2', '女', '女', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('193', 'SYS_LIBRARY_NATION', 'SYS_LIBRARY_NATION:1', '汉族', '汉族', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('194', 'SYS_LIBRARY_NATION', 'SYS_LIBRARY_NATION:2', '蒙古族', '蒙古族', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('195', 'SYS_LIBRARY_NATION', 'SYS_LIBRARY_NATION:3', '回族', '回族', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('196', 'SYS_LIBRARY_NATION', 'SYS_LIBRARY_NATION:4', '藏族', '藏族', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('197', 'SYS_LIBRARY_NATION', 'SYS_LIBRARY_NATION:5', '维吾尔族', '维吾尔族', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('198', 'AGENT_LEVEL_CODE', 'AGENT_LEVEL_CODE:1', '金牌销售', '金牌销售', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('199', 'AGENT_LEVEL_CODE', 'AGENT_LEVEL_CODE:2', '一级销售', '一级销售', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('200', 'AGENT_LEVEL_CODE', 'AGENT_LEVEL_CODE:3', '二级销售', '二级销售', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('201', 'AGENT_LEVEL_CODE', 'AGENT_LEVEL_CODE:4', '三级销售', '三级销售', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('202', 'isSuccessName', 'CBS_ANSWER_STATUS:1', '成功', 's', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('203', 'isSuccessName', 'CBS_ANSWER_STATUS:2', '失败', 'f', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('204', 'isSuccessName', 'CBS_ANSWER_STATUS:3', '无需回访', 'n', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('205', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:7', '满期件', '满期件', '9', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('206', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:8', '失效件', '失效件', '10', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('207', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:10', '停效件', '停效件', '12', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('208', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:12', '契撤件', '契撤件', '14', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('209', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:13', '撤单件', '撤单件', '15', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('210', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:14', '终止件', '终止件', '16', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('211', 'CBS_ACK_TYPE', 'CBS_ACK_TYPE:1', '已签收', '已签收', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('212', 'CBS_ACK_TYPE', 'CBS_ACK_TYPE:2', '未签收', '未签收', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('213', 'CBS_ACK_TYPE', 'CBS_ACK_TYPE:3', '无需回执', '无需回执', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('214', 'CBS_INSURED_TYPE', 'CBS_INSURED_TYPE:0', '自然人', '自然人', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('215', 'CBS_INSURED_TYPE', 'CBS_INSURED_TYPE:1', '法人', '法人', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('216', 'TRENCH_COMMISSION_TYPE', 'TRENCH_COMMISSION_TYPE:1', '个人首续年佣金', '个人首续年佣金', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('217', 'TRENCH_COMMISSION_TYPE', 'TRENCH_COMMISSION_TYPE:3', '渠道推荐费用', '渠道推荐费用', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('218', 'agentGender', 'SYS_LIBRARY_GENDER:0', '未知性别', '弄哦', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('219', 'agentGender', 'SYS_LIBRARY_GENDER:1', '男性', 'nan', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('220', 'agentGender', 'SYS_LIBRARY_GENDER:2', '女性', '1', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('221', 'exclusiveGrade', 'EXCLUSIVE_AGENT_GRADE:1', '合伙人', '合伙人', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('222', 'exclusiveGrade', 'EXCLUSIVE_AGENT_GRADE:2', '高级合伙人', '高级合伙人', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('223', 'exclusiveGrade', 'EXCLUSIVE_AGENT_GRADE:3', '资深合伙人', '资深合伙人', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('224', 'exclusiveLevel', 'EXCLUSIVE_AGENT_LEVEL:1', '1个勋章', '1个勋章', '1', 'EXCLUSIVE_AGENT_GRADE:1', null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('225', 'exclusiveLevel', 'EXCLUSIVE_AGENT_LEVEL:2', '2个勋章', '2个勋章', '2', 'EXCLUSIVE_AGENT_GRADE:1', null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('226', 'exclusiveLevel', 'EXCLUSIVE_AGENT_LEVEL:3', '3个勋章', '3个勋章', '3', 'EXCLUSIVE_AGENT_GRADE:1', null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('227', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:1', '待初审', '待初审', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('228', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:2', '初审', '初审', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('229', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:3', '初审驳回', '初审驳回', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('230', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:4', '待复审', '复审', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('231', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:5', '复审驳回', '复审驳回', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('232', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:6', '复审通过', '复审通过', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('233', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:21', '发票待提交审核', '发票待提交审核', '21', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('234', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:22', '发票已提交审核', '发票已提交审核', '22', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('235', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:23', '发票结算已审核', '发票结算已审核', '23', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('236', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:24', '发票财务已审核', '发票财务已审核', '24', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('237', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:25', '发票税务已审核', '发票税务已审核', '25', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('238', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:26', '发票已归档', '发票已归档', '26', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('239', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:27', '已付款', '已付款', '27', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('240', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:28', '已收款', '已收款', '28', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('241', 'CBS_SETTLEMENT_STATUS', 'CBS_SETTLEMENT_STATUS:7', '结算驳回', '结算驳回', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('242', 'CBS_SELL_WAY', 'CBS_SELL_WAY:3', '平台件', '平台件', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('243', 'cerType', 'SMIS_Sales_CrederType:1', '资格证', '资格证', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('244', 'cerType', 'SMIS_Sales_CrederType:2', '执业证', '执业证', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('245', 'SYS_LIBRARY_PAY_MODE', 'SYS_LIBRARY_PAY_MODE:1', '无关', '无关', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('246', 'SYS_LIBRARY_PAY_MODE', 'SYS_LIBRARY_PAY_MODE:2', '现金', '现金', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('247', 'SYS_LIBRARY_PAY_MODE', 'SYS_LIBRARY_PAY_MODE:4', '转账', '转账', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('248', 'SYS_LIBRARY_PAY_MODE', 'SYS_LIBRARY_PAY_MODE:5', '网上支付', '网上支付', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('249', 'SYS_LIBRARY_PAY_MODE', 'SYS_LIBRARY_PAY_MODE:6', '保险公司支付', '保险公司支付', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('250', 'SYS_LIBRARY_PAY_MODE', 'SYS_LIBRARY_PAY_MODE:8', '划扣', '划扣', '8', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('251', 'SYS_LIBRARY_PAY_MODE', 'SYS_LIBRARY_PAY_MODE:14', '支付宝', '支付宝', '14', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('252', 'SYS_LIBRARY_PAY_MODE', 'SYS_LIBRARY_PAY_MODE:15', '微信支付', '微信支付', '15', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('253', 'recommendGender', 'GENDER:1', '男', 'man', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('254', 'recommendGender', 'GENDER:2', '女', 'woman', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('255', 'recommendGender', 'GENDER:0', '未知', 'not know', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('256', 'chargeTypeName', 'PDT_LIB_CHARGETYPE:1', '年交', 'year', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('257', 'chargeTypeName', 'PDT_LIB_CHARGETYPE:2', '半年交', 'half year', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('258', 'chargeTypeName', 'PDT_LIB_CHARGETYPE:5', '一次交清', 'once', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('259', 'chargeTypeName', 'PDT_LIB_CHARGETYPE:6', '不定期交', 'not sure', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('260', 'SYS_BENE_TYPE', 'SYS_BENE_TYPE:1', '法定受益人', '法定受益人', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('261', 'SYS_BENE_TYPE', 'SYS_BENE_TYPE:2', '指定受益人', '指定受益人', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('262', 'SYS_BENE_TYPE', 'SYS_BENE_TYPE:3', '被保人本人', '被保人本人', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('263', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:11', '身份证', '身份证', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('264', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:12', '户口薄', '户口薄', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('265', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:13', '驾驶证', '驾驶证', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('266', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:14', '军官证', '军官证', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('267', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:15', '士兵证', '士兵证', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('268', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:16', '军官离退休证', '军官离退休证', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('269', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:17', '中国护照', '中国护照', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('270', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:21', '外国护照', '外国护照', '8', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('271', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:22', '旅行证', '旅行证', '9', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('272', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:23', '回乡证', '回乡证', '10', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('273', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:24', '居留证件', '居留证件', '11', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('274', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:29', '其他个人证件', '其他个人证件', '12', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('275', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:31', '组织机构代码证', '组织机构代码证', '13', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('276', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:32', '工商登记证', '工商登记证', '14', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('277', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:33', '税务登记证', '税务登记证', '15', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('278', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:34', '营业执照', '营业执照', '16', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('279', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:99', '其他证件', '其他证件', '17', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('280', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:55', '护照', '护照', '18', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('281', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:56', '港澳通行证', '港澳通行证', '19', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('282', 'SYS_LIBRARY_CERTI_TYPE', 'SYS_LIBRARY_CERTI_TYPE:57', '台胞证', '台胞证', '20', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('283', 'CBS_APPLICANT_RELATION2', 'CBS_APPLICANT_RELATION2:0', '无关或不确定', '无关或不确定', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('284', 'CBS_APPLICANT_RELATION2', 'CBS_APPLICANT_RELATION2:1', '配偶', '配偶', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('285', 'CBS_APPLICANT_RELATION2', 'CBS_APPLICANT_RELATION2:2', '子女', '子女', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('286', 'CBS_APPLICANT_RELATION2', 'CBS_APPLICANT_RELATION2:3', '父母', '父母', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('287', 'CBS_APPLICANT_RELATION2', 'CBS_APPLICANT_RELATION2:4', '亲属', '亲属', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('288', 'CBS_APPLICANT_RELATION2', 'CBS_APPLICANT_RELATION2:5', '本人', '本人', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('289', 'CBS_APPLICANT_RELATION2', 'CBS_APPLICANT_RELATION2:6', '其它', '其它', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('290', 'CBS_APPLICANT_RELATION2', 'CBS_APPLICANT_RELATION2:7', '雇佣关系', '雇佣关系', '8', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('291', 'CBS_APPLICANT_RELATION2', 'CBS_APPLICANT_RELATION2:8', '金融机构', '金融机构', '9', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('292', 'SYS_LIBRARY_MONEYTYPE', 'SYS_LIBRARY_MONEYTYPE:1', '人民币', '人民币', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('293', 'IS_ACHIEVEMENT', 'IS_ACHIEVEMENT:0', '是', '是', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('294', 'IS_ACHIEVEMENT', 'IS_ACHIEVEMENT:1', '否', '否', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('295', 'PDT_PRODUCT_INSTYPE', 'PDT_PRODUCT_INSTYPE:1', '主险', '主险', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('296', 'PDT_PRODUCT_INSTYPE', 'PDT_PRODUCT_INSTYPE:0', '附加险', '附加险', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('297', 'POLICY_ATTACH_TYPE', 'POLICY_ATTACH_TYPE:1', '身份证影像件(正)', '身份证影像件(正)', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('298', 'POLICY_ATTACH_TYPE', 'POLICY_ATTACH_TYPE:2', '身份证影像件(反)', '身份证影像件(反)', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('299', 'POLICY_ATTACH_TYPE', 'POLICY_ATTACH_TYPE:3', '银行卡影像件(正)', '银行卡影像件(正)', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('300', 'POLICY_ATTACH_TYPE', 'POLICY_ATTACH_TYPE:4', '银行卡影像件(反)', '银行卡影像件(反)', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('301', 'POLICY_ATTACH_TYPE', 'POLICY_ATTACH_TYPE:5', '投保单影像件', '投保单影像件', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('302', 'POLICY_ATTACH_TYPE', 'POLICY_ATTACH_TYPE:6', '保单影像件', '保单影像件', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('303', 'POLICY_ATTACH_TYPE', 'POLICY_ATTACH_TYPE:7', '回执影像件', '回执影像件', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('304', 'contractState', 'CONT_CONTRACT_STATE:0', '未提交审核', '未提交审核', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('305', 'contractState', 'CONT_CONTRACT_STATE:1', '待审核', '待审核', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('306', 'contractState', 'CONT_CONTRACT_STATE:2', '审核通过', '审核通过', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('307', 'contractState', 'CONT_CONTRACT_STATE:3', '审核驳回', '审核驳回', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('308', 'contractState', 'CONT_CONTRACT_STATE:4', '失效', '失效', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('309', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:1', '专业技术人员', '专业技术人员', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('310', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:2', '国家机关及其工作机构负责人', '国家机关及其工作机构负责人', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('311', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:3', '民主党派和社会团体及其工作机构负责人', '民主党派和社会团体及其工作机构负责人', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('312', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:4', '事业单位负责人', '事业单位负责人', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('313', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:5', '企业负责人', '企业负责人', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('314', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:13', '工程技术人员', '工程技术人员', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('315', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:17', '农业技术人员', '农业技术人员', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('316', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:18', '飞机和船舶技术人员', '飞机和船舶技术人员', '8', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('317', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:19', '卫生专业技术人员', '卫生专业技术人员', '9', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('318', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:21', '经济业务人员', '经济业务人员', '10', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('319', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:22', '金融业务人员', '金融业务人员', '11', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('320', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:23', '法律专业人员', '法律专业人员', '12', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('321', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:24', '教学人员', '教学人员', '13', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('322', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:25', '文学艺术工作人员', '文学艺术工作人员', '14', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('323', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:26', '体育工作人员', '体育工作人员', '15', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('324', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:27', '新闻出版、文化工作人员', '新闻出版、文化工作人员', '16', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('325', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:28', '宗教职业者', '宗教职业者', '17', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('326', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:29', '其他专业技术人员', '其他专业技术人员', '18', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('327', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:30', '办事人员和有关人员', '办事人员和有关人员', '19', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('328', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:31', '行政办公人员', '行政办公人员', '20', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('329', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:32', '安全保卫和消防人员', '安全保卫和消防人员', '21', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('330', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:33', '邮政和电信业务人员', '邮政和电信业务人员', '22', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('331', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:39', '其他办事人员和有关人员', '其他办事人员和有关人员', '23', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('332', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:41', '购销人员', '购销人员', '24', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('333', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:42', '仓储人员', '仓储人员', '25', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('334', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:43', '餐馆服务人员', '餐馆服务人员', '26', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('335', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:44', '饭店、旅游及健身娱乐场所服务人员', '饭店、旅游及健身娱乐场所服务人员', '27', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('336', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:45', '运输服务人员', '运输服务人员', '28', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('337', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:46', '医疗卫生辅助服务人员', '医疗卫生辅助服务人员', '29', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('338', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:47', '社会服务和居民生活服务人员', '社会服务和居民生活服务人员', '30', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('339', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:49', '其他商业、服务业人员', '其他商业、服务业人员', '31', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('340', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:51', '种植业生产人员', '种植业生产人员', '32', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('341', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:52', '林业生产及野生动物保护人员', '林业生产及野生动物保护人员', '33', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('342', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:53', '畜牧业生产人员', '畜牧业生产人员', '34', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('343', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:54', '渔业生产人员', '渔业生产人员', '35', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('344', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:55', '水利设备管理养护人员', '水利设备管理养护人员', '36', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('345', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:59', '其他农、林、牧、鱼、水利业生产人员', '其他农、林、牧、鱼、水利业生产人员', '37', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('346', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:61', '勘测及矿物开采人员', '勘测及矿物开采人员', '38', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('347', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:62', '金属冶炼、轧制人员', '金属冶炼、轧制人员', '39', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('348', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:64', '化工产品生产人员', '化工产品生产人员', '40', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('349', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:66', '机械制造加工人员', '机械制造加工人员', '41', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('350', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:71', '机械设备修理人员', '机械设备修理人员', '42', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('351', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:72', '电力设备安装、运行、检修及供电人员', '电力设备安装、运行、检修及供电人员', '43', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('352', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:73', '电子元器件与设备制造、装配、调试及维修人员', '电子元器件与设备制造、装配、调试及维修人员', '44', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('353', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:74', '橡胶和塑料制品生产人员', '橡胶和塑料制品生产人员', '45', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('354', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:75', '纺织、针织、印染人员', '纺织、针织、印染人员', '46', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('355', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:76', '裁剪、缝纫和皮革、毛皮制品加工制作人员', '裁剪、缝纫和皮革、毛皮制品加工制作人员', '47', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('356', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:77', '粮油、食品、饮料生产及饮料生产加工人员', '粮油、食品、饮料生产及饮料生产加工人员', '48', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('357', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:78', '烟草及其制品加工人员', '烟草及其制品加工人员', '49', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('358', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:79', '药品生产人员', '药品生产人员', '50', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('359', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:81', '木材加工、人造板生产、木制品制作及制浆、造纸和纸制品生产加工人员', '木材加工、人造板生产、木制品制作及制浆、造纸和纸制品生产加工人员', '51', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('360', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:82', '建筑材料生产加工人员', '建筑材料生产加工人员', '52', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('361', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:83', '玻璃、陶瓷、搪瓷及其制品生产加工人员', '玻璃、陶瓷、搪瓷及其制品生产加工人员', '53', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('362', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:84', '广播影视制品制作、播放及文物保护作业人员', '广播影视制品制作、播放及文物保护作业人员', '54', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('363', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:85', '印刷人员', '印刷人员', '55', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('364', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:86', '工艺、美术品制作人员', '工艺、美术品制作人员', '56', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('365', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:87', '文化教育、体育用品制作人员', '文化教育、体育用品制作人员', '57', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('366', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:88', '工程施工人员', '工程施工人员', '58', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('367', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:91', '运输设备操作人员及有关人员', '运输设备操作人员及有关人员', '59', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('368', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:92', '环境监测及废物处理人员', '环境监测及废物处理人员', '60', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('369', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:93', '检验、计量人员', '检验、计量人员', '61', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('370', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:99', '其他生产、运输设备操作人员及有关人员', '其他生产、运输设备操作人员及有关人员', '62', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('371', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:x', '军人', '军人', '63', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('372', 'SYS_LIBRARY_JOBTYPE', 'SYS_LIBRARY_JOBTYPE:y', '不便分类的其他从业人员', '不便分类的其他从业人员', '64', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('373', 'SYS_LIBRARY_COMPANY_CERTI_TYPE', 'SYS_LIBRARY_COMPANY_CERTI_TYPE:0', '统一社会信用代码', '统一社会信用代码', '0', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('374', 'SYS_LIBRARY_COMPANY_CERTI_TYPE', 'SYS_LIBRARY_COMPANY_CERTI_TYPE:1', '组织机构代码', '组织机构代码', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('375', 'PDT_LIB_COVERAGEPERIOD', 'PDT_LIB_COVERAGEPERIOD:0', '无关', '无关', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('376', 'PDT_LIB_COVERAGEPERIOD', 'PDT_LIB_COVERAGEPERIOD:1', '保终身', '保终身', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('377', 'PDT_LIB_COVERAGEPERIOD', 'PDT_LIB_COVERAGEPERIOD:2', '按年限保', '按年限保', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('378', 'PDT_LIB_COVERAGEPERIOD', 'PDT_LIB_COVERAGEPERIOD:3', '保至某确定年龄', '保至某确定年龄', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('379', 'PDT_LIB_COVERAGEPERIOD', 'PDT_LIB_COVERAGEPERIOD:4', '按月保', '按月保', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('380', 'PDT_LIB_COVERAGEPERIOD', 'PDT_LIB_COVERAGEPERIOD:5', '按天保', '按天保', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('381', 'PDT_LIB_CHARGETYPE', 'PDT_LIB_CHARGETYPE:1', '年交', '年交', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('382', 'PDT_LIB_CHARGETYPE', 'PDT_LIB_CHARGETYPE:2', '半年交', '半年交', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('383', 'PDT_LIB_CHARGETYPE', 'PDT_LIB_CHARGETYPE:3', '季交', '季交', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('384', 'PDT_LIB_CHARGETYPE', 'PDT_LIB_CHARGETYPE:4', '月交', '月交', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('385', 'PDT_LIB_CHARGETYPE', 'PDT_LIB_CHARGETYPE:5', '一次性交清', '一次性交清', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('386', 'PDT_LIB_CHARGETYPE', 'PDT_LIB_CHARGETYPE:6', '不定期交', '不定期交', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('387', 'CAR_USE_TYPE', 'CAR_USE_TYPE:0', '非营运', '非营运', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('388', 'CAR_USE_TYPE', 'CAR_USE_TYPE:1', '营运', '营运', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('389', 'CAR_TYPE', 'CAR_TYPE:1', '6-9座客车', '6-9座客车', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('390', 'CAR_TYPE', 'CAR_TYPE:2', '5座及以下', '5座及以下', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('391', 'DRIVING_LISENCE_SORT', 'DRIVING_LISENCE_SORT:0', 'A1', 'A1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('392', 'DRIVING_LISENCE_SORT', 'DRIVING_LISENCE_SORT:1', 'A2', 'A2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('393', 'DRIVING_LISENCE_SORT', 'DRIVING_LISENCE_SORT:2', 'A3', 'A3', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('394', 'DRIVING_LISENCE_SORT', 'DRIVING_LISENCE_SORT:3', 'B1', 'B1', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('395', 'DRIVING_LISENCE_SORT', 'DRIVING_LISENCE_SORT:4', 'B2', 'B2', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('396', 'DRIVING_LISENCE_SORT', 'DRIVING_LISENCE_SORT:5', 'C1', 'C1', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('397', 'DRIVING_LISENCE_SORT', 'DRIVING_LISENCE_SORT:6', 'C2', 'C2', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('398', 'stContractSettleType', 'TRENCH_CONTRACT_SETTLE_TYPE:1', '正常结算', '正常结算	', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('399', 'stContractSettleType', 'TRENCH_CONTRACT_SETTLE_TYPE:2', '提前结算', '提前结算', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('400', 'extensionPeriod', 'PDT_EXTENSION_PERIOD:1', '1', '1', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('401', 'extensionPeriod', 'PDT_EXTENSION_PERIOD:2', '2', '2', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('402', 'extensionPeriod', 'PDT_EXTENSION_PERIOD:3', '3', '3', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('403', 'extensionPeriod', 'PDT_EXTENSION_PERIOD:4', '4', '4', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('404', 'extensionPeriod', 'PDT_EXTENSION_PERIOD:5', '5', '5', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('405', 'extensionPeriod', 'PDT_EXTENSION_PERIOD:99', '永久', '99', '99', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('406', 'SET_PLDE_ITEM', 'SET_PLDE_ITEM:0', '部门费用', '部门费用', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('407', 'SET_PLDE_ITEM', 'SET_PLDE_ITEM:6', '退保保单佣金', '退保保单佣金', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('408', 'SET_PLDE_ITEM', 'SET_PLDE_ITEM:7', '其他', '其他', '8', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('409', 'SET_PLDE_TYPE', 'SET_PLDE_TYPE:0', '加款', '加款', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('410', 'SET_PLDE_TYPE', 'SET_PLDE_TYPE:1', '扣款', '扣款', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('411', 'commissionCalculateBase', 'PDT_COMMISSION_CALCULATE_BASE:3', '规模保费', 'ss', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('412', 'commissionType', 'TRENCH_COMMISSION_TYPE:1', '个人首续年佣金', 'ss', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('413', 'commissionType', 'TRENCH_COMMISSION_TYPE:3', '渠道推荐费用', 'ss', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('414', 'commissionCalculateType', 'TRENCH_COMMISSION_CALCULATE_TYPE:1', '按保费计算', 'ss', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('415', 'commissionCalculateType', 'TRENCH_COMMISSION_CALCULATE_TYPE:2', '保费合计达成', 'ss', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('416', 'commissionCalculateType', 'TRENCH_COMMISSION_CALCULATE_TYPE:3', '保费合计达成-累进', 'ss', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('417', 'commissionCalculateType', 'TRENCH_COMMISSION_CALCULATE_TYPE:7', '按件数计算', 'ss', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('418', 'totalCycle', 'PDT_TOTAL_CYCLE:1', '自然年', 'ss', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('419', 'totalCycle', 'PDT_TOTAL_CYCLE:2', '自然季度', 'ss', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('420', 'totalCycle', 'PDT_TOTAL_CYCLE:3', '自然月', 'ss', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('421', 'totalCycle', 'PDT_TOTAL_CYCLE:4', '协议存续期', 'ss', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('422', 'totalCycle', 'PDT_TOTAL_CYCLE:5', '结算月', 'ss', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('423', 'rangeUnit', 'PDT_RANGE_UNIT:1', '元', 'ss', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('424', 'rangeUnit', 'PDT_RANGE_UNIT:2', '%', 'ss', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('425', 'rangeUnit', 'PDT_RANGE_UNIT:3', '人', 'ss', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('426', 'CONT_BASELAW_STATE', 'CONT_BASELAW_STATE:0', '未提交审核', '未提交审核', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('427', 'CONT_BASELAW_STATE', 'CONT_BASELAW_STATE:1', '待审核', '待审核', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('428', 'CONT_BASELAW_STATE', 'CONT_BASELAW_STATE:2', '审核通过', '审核通过', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('429', 'CONT_BASELAW_STATE', 'CONT_BASELAW_STATE:3', '审核驳回', '审核驳回', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('430', 'BASICLAW_COST_OWNER', 'BASICLAW_COST_OWNER:1', '推荐人', '推荐人', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('431', 'BASICLAW_COST_OWNER', 'BASICLAW_COST_OWNER:2', '增员人', '增员人', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('432', 'BASICLAW_COST_OWNER', 'BASICLAW_COST_OWNER:3', '代理人', '代理人', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('433', 'BL_COMMISSION_CALCULATE_TYPE', '1', '按保费计算', '按保费计算', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('434', 'BL_COMMISSION_CALCULATE_TYPE', '2', '同主险', '同主险', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('435', 'BASICLAW_COMMISSION_TYPE', 'BASICLAW_COMMISSION_TYPE:1', '直销佣金', '直销佣金2', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('436', 'BASICLAW_COMMISSION_TYPE', 'BASICLAW_COMMISSION_TYPE:2', '激励佣金', '激励佣金', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('437', 'BASICLAW_COMMISSION_TYPE', 'BASICLAW_COMMISSION_TYPE:3', '续期佣金', '续期佣金', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('438', 'BASICLAW_COMMISSION_TYPE', 'BASICLAW_COMMISSION_TYPE:4', '推荐佣金', '推荐佣金2', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('439', 'BASICLAW_COMMISSION_TYPE', 'BASICLAW_COMMISSION_TYPE:5', '服务佣金', '服务佣金', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('440', 'BASICLAW_COMMISSION_TYPE', 'BASICLAW_COMMISSION_TYPE:6', '增员佣金', '增员佣金', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('441', 'BASICLAW_APPRAISE_SUBJECTS', '1', '金币', '金币', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('442', 'BL_APP_SUBJECTS_CALCULATE_TYPE', '1', '求和', '求和', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('443', 'BASICLAW_APPRAISE_CYCLE', '1', '自然日', '自然日', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('444', 'BASICLAW_APPRAISE_CYCLE', '2', '自然季度', '自然季度', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('445', 'PDT_ONLINE_STATUS', 'PDT_ONLINE_STATUS:1', '已上线', '已上线', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('446', 'PDT_ONLINE_STATUS', 'PDT_ONLINE_STATUS:2', '未上线', '未上线', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('447', 'PDT_PRODUCT_TYPE1', 'PDT_PRODUCT_TYPE1:0', '个险', '个险', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('448', 'PDT_PRODUCT_TYPE1', 'PDT_PRODUCT_TYPE1:1', '团险', '团险', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('449', 'PDT_PRODUCT_TYPE1', 'PDT_PRODUCT_TYPE1:2', '财险', '财险', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('450', 'PDT_PRODUCT_TYPE1', 'PDT_PRODUCT_TYPE1:3', '车险', '车险', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('451', 'productType', 'PDT_PRODUCT_TYPE1:0', '个险', 'ss', '0', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('452', 'productType', 'PDT_PRODUCT_TYPE1:1', '团险', 'ss', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('453', 'productType', 'PDT_PRODUCT_TYPE1:2', '财险', 'ss', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('454', 'productType', 'PDT_PRODUCT_TYPE1:3', '车险', 'ss', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('455', 'onlineStatus', '1', '已上线', 'ss', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('456', 'onlineStatus', '2', '未上线', 'ss', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('457', 'PDT_LIB_CHARGEPERIOD', 'PDT_LIB_CHARGEPERIOD:1', '无关', '无关', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('458', 'PDT_LIB_CHARGEPERIOD', 'PDT_LIB_CHARGEPERIOD:2', '一次性交清', '一次性交清', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('459', 'PDT_LIB_CHARGEPERIOD', 'PDT_LIB_CHARGEPERIOD:3', '按年限交', '按年限交', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('460', 'PDT_LIB_CHARGEPERIOD', 'PDT_LIB_CHARGEPERIOD:4', '交至某确定年龄', '交至某确定年龄', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('461', 'PDT_LIB_CHARGEPERIOD', 'PDT_LIB_CHARGEPERIOD:6', '按月交', '按月交', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('462', 'SET_DIFF_TYPE', 'SET_DIFF_TYPE:0', '渠道多', '渠道多', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('463', 'SET_DIFF_TYPE', 'SET_DIFF_TYPE:1', '代理公司多', '代理公司多', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('464', 'SET_DIFF_TYPE', 'SET_DIFF_TYPE:2', '保险公司多', '保险公司多', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('465', 'SET_DIFF_STATUS', 'SET_DIFF_STATUS:1', '待确认', '待确认', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('466', 'SET_DIFF_STATUS', 'SET_DIFF_STATUS:2', '已忽略', '已忽略', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('467', 'SET_DIFF_STATUS', 'SET_DIFF_STATUS:3', '已确认', '已确认', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('468', 'CBS_POLICY_TYPE_NEW', 'CBS_POLICY_TYPE:0', '个险', '个险', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('469', 'CBS_POLICY_TYPE_NEW', 'CBS_POLICY_TYPE:1', '团险', '团险', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('470', 'CBS_POLICY_TYPE_NEW', 'CBS_POLICY_TYPE:2', '财险', '财险', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('471', 'CBS_POLICY_TYPE_NEW', 'CBS_POLICY_TYPE:3', '车险', '车险', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('472', 'CBS_ANSWER_TYPE', 'CBS_ANSWER_TYPE:0', '不回访', '不回访', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('473', 'CBS_ANSWER_TYPE', 'CBS_ANSWER_TYPE:1', '电话回访', '电话回访', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('474', 'CBS_ANSWER_TYPE', 'CBS_ANSWER_TYPE:2', '网上回访', '网上回访', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('475', 'CBS_ANSWER_TYPE', 'CBS_ANSWER_TYPE:3', '人工回访', '人工回访', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('476', 'CBS_ANSWER_TYPE', 'CBS_ANSWER_TYPE:9', '其他', '其他', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('477', 'CBS_ANSWER_STATUS', 'CBS_ANSWER_STATUS:0', '成功', '成功', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('478', 'CBS_ANSWER_STATUS', 'CBS_ANSWER_STATUS:1', '失败', '失败', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('479', 'CBS_ANSWER_STATUS', 'CBS_ANSWER_STATUS:2', '无需回访', '无需回访', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('480', 'BASICLAW_GRADE_STAFF_FLAG', 'BASICLAW_GRADE_STAFF_FLAG:1', '专属代理人', '专属代理人', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('481', 'BASICLAW_GRADE_STAFF_FLAG', 'BASICLAW_GRADE_STAFF_FLAG:2', '推荐人', '推荐人', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('482', 'BASICLAW_GRADE_STAFF_FLAG', 'BASICLAW_GRADE_STAFF_FLAG:3', '推荐人的增员人', '推荐人的增员人', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('483', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:32', '核保失败（系统状态）', '核保失败（系统状态）', '23', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('484', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:33', '核保通过（系统状态）', '核保通过（系统状态）', '24', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('485', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:34', '承保失败（系统状态）', '承保失败（系统状态）', '26', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('486', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:35', '人工核保（系统状态）', '人工核保（系统状态）', '25', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('487', 'CBS_CONTRACTLIFE_STATUS', 'CBS_CONTRACTLIFE_STATUS:38', '协议退保', '协议退保', '27', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('488', 'BASICLAW_GOLD_TYPE', 'BASICLAW_GOLD_TYPE:1', '金币_签单奖励', '金币_签单奖励', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('489', 'BASICLAW_GOLD_TYPE', 'BASICLAW_GOLD_TYPE:1', '金币_签单奖励', '金币_签单奖励', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('490', 'BASICLAW_GOLD_TYPE', 'BASICLAW_GOLD_TYPE:2', '金币_推荐奖励', '金币_推荐奖励', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('491', 'BASICLAW_GOLD_TYPE', 'BASICLAW_GOLD_TYPE:3', '金币_增员奖励', '金币_增员奖励', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('492', 'BASICLAW_GOLD_TYPE', 'BASICLAW_GOLD_TYPE:4', '金币_初始奖励', '金币_初始奖励', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('493', 'BASICLAW_GOLD_TYPE', 'BASICLAW_GOLD_TYPE:5', '金币_任务奖励', '金币_任务奖励', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('494', 'problemStatus', 'CBS_PROBLEM_STATUS:3', '已回复', 'problemStatus', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('495', 'problemStatus', 'CBS_PROBLEM_STATUS:4', '已回销', '4', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('496', 'problemStatus', 'CBS_PROBLEM_STATUS:5', '异常回销', '5', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('497', 'PDT_CONTRACT_TYPE3', 'PDT_CONTRACT_TYPE3:1', '保险公司协议', '保险公司协议', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('498', 'PDT_CONTRACT_TYPE3', 'PDT_CONTRACT_TYPE3:2', '渠道协议', '渠道协议', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('499', 'TRENCH_COMMISSION_CALCULATE_TYPE', 'TRENCH_COMMISSION_CALCULATE_TYPE:1', '按保费计算', '按保费计算', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('500', 'TRENCH_COMMISSION_CALCULATE_TYPE', 'TRENCH_COMMISSION_CALCULATE_TYPE:2', '保费合计达成', '保费合计达成', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('501', 'TRENCH_COMMISSION_CALCULATE_TYPE', 'TRENCH_COMMISSION_CALCULATE_TYPE:3', '保费合计达成-累进', '保费合计达成-累进', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('502', 'TRENCH_COMMISSION_CALCULATE_TYPE', 'TRENCH_COMMISSION_CALCULATE_TYPE:7', '按件数计算', '按件数计算', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('503', 'COMPANY_COMMISSION_CALCULATE_TYPE', 'COMPANY_COMMISSION_CALCULATE_TYPE:1', '按保费计算', '按保费计算', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('504', 'COMPANY_COMMISSION_CALCULATE_TYPE', 'COMPANY_COMMISSION_CALCULATE_TYPE:2', '保费合计达成', '保费合计达成', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('505', 'COMPANY_COMMISSION_CALCULATE_TYPE', 'COMPANY_COMMISSION_CALCULATE_TYPE:3', '保费合计达成-累进', '保费合计达成-累进', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('506', 'COMPANY_COMMISSION_CALCULATE_TYPE', 'COMPANY_COMMISSION_CALCULATE_TYPE:7', '按件数计算', '按件数计算', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('507', 'PDT_RANGE_UNIT', 'PDT_RANGE_UNIT:1', '元', '元', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('508', 'PDT_RANGE_UNIT', 'PDT_RANGE_UNIT:2', '%', '%', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('509', 'PDT_RANGE_UNIT', 'PDT_RANGE_UNIT:3', '人', '人', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('510', 'PDT_COMMISSION_CALCULATE_BASE', 'PDT_COMMISSION_CALCULATE_BASE:3', '规模保费', '规模保费', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('511', 'PDT_TOTAL_CYCLE', 'PDT_TOTAL_CYCLE:1', '自然年', '自然年', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('512', 'PDT_TOTAL_CYCLE', 'PDT_TOTAL_CYCLE:2', '自然季度', '自然季度', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('513', 'PDT_TOTAL_CYCLE', 'PDT_TOTAL_CYCLE:3', '自然月', '自然月', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('514', 'PDT_TOTAL_CYCLE', 'PDT_TOTAL_CYCLE:4', '协议存续期', '协议存续期', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('515', 'PDT_TOTAL_CYCLE', 'PDT_TOTAL_CYCLE:5', '结算月', '结算月', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('516', 'CONT_CONTRACT_STATE', 'CONT_CONTRACT_STATE:0', '未提交审核', '未提交审核', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('517', 'CONT_CONTRACT_STATE', 'CONT_CONTRACT_STATE:1', '待审核', '待审核', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('518', 'CONT_CONTRACT_STATE', 'CONT_CONTRACT_STATE:2', '审核通过', '审核通过', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('519', 'CONT_CONTRACT_STATE', 'CONT_CONTRACT_STATE:3', '审核驳回', '审核驳回', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('520', 'CONT_CONTRACT_STATE', 'CONT_CONTRACT_STATE:4', '失效', '失效', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('521', 'ROLE_TYPE', 'ROLE_TYPE:0', '系统管理员', '系统管理员', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('522', 'ROLE_TYPE', 'ROLE_TYPE:1', '渠道管理员', '渠道管理员', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('523', 'ROLE_TYPE', 'ROLE_TYPE:2', '子渠道管理员', '子渠道管理员', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('524', 'ROLE_TYPE', 'ROLE_TYPE:3', '合作者', '合作者', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('525', 'CBS_SELL_WAY', 'CBS_SELL_WAY:0', '网销件', '网销件', '0', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('526', 'CBS_SELL_WAY', 'CBS_SELL_WAY:1', '非网销件', '非网销件', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('527', 'CTPORFOLIOCODE', 'RBCX000000RBCX000001', '微博钱包账户安全险', 'qb', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('528', 'CTPORFOLIOCODE', 'RBCX000000RBCX000002', '微博用户个人金库保镖', 'bb', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('529', 'CTPORFOLIOCODE', 'RBCX000000RBCX000003', '面部诊疗意外保险', 'mb', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('530', 'CTPORFOLIOCODE', 'RBCX000000RBCX000004', '眼部整形手术意外保险', 'yb', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('531', 'CTPORFOLIOCODE', 'RBCX000000RBCX000005', '鼻部手术整形意外保险', 'bb', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('532', 'SERVICE_CODE', 'cashier', '收银台', 'cashier', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('533', 'SERVICE_CODE', 'edorCT', '退保服务', '退保服务', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('534', 'test_key', '01', '测试', 'test01', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('535', 'SYS_PAY_CHANNEL', 'easylink', '易联支付', '易联支付', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('536', 'SYS_PAY_CHANNEL', 'sina', '新浪支付', 'sina', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('537', 'PDT_COMMISSION_CALCULATE_BASE', 'PDT_COMMISSION_CALCULATE_BASE:3', '净保费', '净保费', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('538', 'commissionType', 'TRENCH_COMMISSION_TYPE:2', '部门奖金', 'ss', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('539', 'TRENCH_COMMISSION_CALCULATE_TYPE', 'TRENCH_COMMISSION_CALCULATE_TYPE:4', '同主险', '同主险', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('540', 'commissionType', 'TRENCH_COMMISSION_TYPE:4', '同主险', '同主险', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('541', 'SERVICE_CLASS', 'SERVICE_CLASS:0', '传统', '传统', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('542', 'POLICY_STATUS', 'POLICY_STATUS:0', '录入', '录入', '0', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('543', 'POLICY_STATUS', 'POLICY_STATUS:1', '计算完成', '计算完成', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('544', 'POLICY_STATUS', 'POLICY_STATUS:2', '已汇总', '已汇总', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('545', 'POLICY_STATUS', 'POLICY_STATUS:3', '正审核', '正审核', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('546', 'POLICY_STATUS', 'POLICY_STATUS:4', '审核完成', '审核完成', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('547', 'POLICY_STATUS', 'POLICY_STATUS:5', '审核不通过', '审核不通过', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('548', 'POLICY_STATUS', 'POLICY_STATUS:6', '已下发', '已下发', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('549', 'POLICY_STATUS', 'POLICY_STATUS:7', '其他', '', '0', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('550', 'agentStatus', '1', '注册成功', '注册成功', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('551', 'agentStatus', '0', '待注册', '待注册', '0', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('552', 'agentStatus', 'A', '注册失败', '注册失败', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('553', 'writeBackStatus', '1', '回写成功', '回写成功', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('554', 'writeBackStatus', '0', '待回写', '待回写', '0', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('555', 'writeBackStatus', 'A', '回写失败', '回写失败', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('556', 'ZHLHsourceId', '0', '自建', '', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('557', 'ZHLHsourceId', '10004', '车商保', '车商保', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('558', 'ZHLHsourceId', '10000', '共享保', '共享保', '0', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('559', 'riskType', '1', '车险', '车险', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('560', 'riskType', '2', '非车险', '非车险', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('561', 'nation', '04', '藏族', '藏族', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('562', 'nation', '05', '维吾尔族', '维吾尔族', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('563', 'nation', '06', '苗族', '苗族', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('564', 'nation', '07', '彝族', '彝族', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('565', 'nation', '08', '壮族', '壮族', '8', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('566', 'nation', '09', '布依族', '布依族', '9', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('567', 'nation', '10', '朝鲜族', '朝鲜族', '10', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('568', 'nation', '11', '满族', '满族', '11', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('569', 'nation', '12', '侗族', '侗族', '12', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('570', 'nation', '13', '瑶族', '瑶族', '13', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('571', 'nation', '14', '白族', '白族', '14', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('572', 'nation', '15', '土家族', '土家族', '15', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('573', 'nation', '16', '哈尼族', '哈尼族', '16', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('574', 'nation', '17', '哈萨克族', '哈萨克族', '17', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('575', 'nation', '18', '傣族', '傣族', '18', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('576', 'nation', '19', '黎族', '黎族', '19', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('577', 'nation', '20', '傈傈族', '傈傈族', '20', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('578', 'nation', '21', '佤族', '佤族', '21', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('579', 'nation', '22', '畲族', '畲族', '22', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('580', 'nation', '23', '高山族', '高山族', '23', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('581', 'nation', '24', '拉祜族', '拉祜族', '24', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('582', 'nation', '25', '水族', '水族', '25', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('583', 'nation', '26', '东乡族', '东乡族', '26', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('584', 'nation', '27', '纳西族', '纳西族', '27', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('585', 'nation', '28', '景颇族', '景颇族', '28', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('586', 'nation', '29', '柯尔克孜族', '柯尔克孜族', '29', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('587', 'nation', '30', '土族', '土族', '30', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('588', 'nation', '31', '达斡尔族', '达斡尔族', '31', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('589', 'nation', '32', '仫佬族', '仫佬族', '32', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('590', 'nation', '33', '羌族', '羌族', '33', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('591', 'nation', '34', '布朗族', '布朗族', '34', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('592', 'nation', '35', '撒拉族', '撒拉族', '35', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('593', 'nation', '36', '毛南族', '毛南族', '36', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('594', 'nation', '37', '仡佬族', '仡佬族', '37', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('595', 'nation', '38', '锡伯族', '锡伯族', '38', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('596', 'nation', '39', '阿昌族', '阿昌族', '39', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('597', 'nation', '40', '普米族', '普米族', '40', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('598', 'nation', '41', '塔吉克族', '塔吉克族', '41', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('599', 'nation', '42', '怒族', '怒族', '42', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('600', 'nation', '43', '乌孜别克族', '乌孜别克族', '43', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('601', 'nation', '44', '俄罗斯族', '俄罗斯族', '44', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('602', 'nation', '45', '鄂温克族', '鄂温克族', '45', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('603', 'nation', '46', '德昂族', '德昂族', '46', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('604', 'nation', '47', '保安族', '保安族', '47', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('605', 'nation', '48', '裕固族', '裕固族', '48', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('606', 'nation', '49', '京族', '京族', '49', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('607', 'nation', '50', '塔塔尔族', '塔塔尔族', '50', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('608', 'nation', '51', '独龙族', '独龙族', '51', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('609', 'nation', '52', '鄂伦春族', '鄂伦春族', '52', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('610', 'nation', '53', '赫哲族', '赫哲族', '53', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('611', 'nation', '54', '门巴族', '门巴族', '54', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('612', 'nation', '55', '珞巴族', '珞巴族', '55', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('613', 'nation', '56', '基诺族', '基诺族', '56', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('614', 'nation', '99', '未知', '未知', '57', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('615', 'nation', '01', '汉族', '汉族', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('616', 'nation', '02', '蒙古族', '蒙古族', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('617', 'nation', '03', '回族', '回族', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('618', 'culture', '11', '博士', '博士', '0', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('619', 'culture', '14', '硕士', '硕士', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('620', 'culture', '17', '研究生', '研究生', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('621', 'culture', '21', '本科', '本科', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('622', 'culture', '31', '大专', '大专', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('623', 'culture', '41', '中专', '中专', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('624', 'culture', '61', '高中及同等学历', '高中及同等学历', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('625', 'culture', '71', '初中及同等学历', '初中及同等学历', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('626', 'culture', '81', '初中以下学历', '初中以下学历', '8', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('627', 'culture', '90', '其他', '其他', '9', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('628', 'polity', '01', '中共党员', '中共党员', '0', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('629', 'polity', '02', '中共预备党员', '中共预备党员', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('630', 'polity', '03', '共青团员', '共青团员', '2', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('631', 'polity', '04', '民革会员', '民革会员', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('632', 'polity', '05', '民盟盟员', '民盟盟员', '4', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('633', 'polity', '06', '民建会员', '民建会员', '5', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('634', 'polity', '07', '民进会员', '民进会员', '6', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('635', 'polity', '08', '农工党党员', '农工党党员', '7', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('636', 'polity', '09', '致公党党员', '致公党党员', '8', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('637', 'polity', '10', '九三学社社员', '九三学社社员', '9', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('638', 'polity', '11', '台盟盟员', '台盟盟员', '10', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('639', 'polity', '12', '无党派人士', '无党派人士', '11', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('640', 'polity', '13', '群众', '群众', '12', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('641', 'riskType', '3', '个险', '个险', '3', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('642', 'riskSpecific', '0', '车驾意', '车驾意', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('643', 'riskClass', '0', '财产险', '财产险', '1', null, null, null, null, null);
INSERT INTO `sys_dictionary` VALUES ('644', 'sad', '123', 'asd2', 'asda', '11', null, null, '2019-08-28 11:05:51', null, '2019-08-28 14:53:51');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(1024) DEFAULT NULL,
  `path` varchar(1024) DEFAULT NULL,
  `path_method` varchar(10) DEFAULT NULL,
  `css` varchar(32) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `type` tinyint(1) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('2', '12', '用户管理', '#!user', 'system/user.html', null, 'layui-icon-friends', '2', '2017-11-17 16:56:59', '2018-09-19 11:26:14', '1', '0');
INSERT INTO `sys_menu` VALUES ('3', '12', '角色管理', '#!role', 'system/role.html', null, 'layui-icon-user', '3', '2017-11-17 16:56:59', '2019-01-14 15:34:40', '1', '0');
INSERT INTO `sys_menu` VALUES ('4', '12', '菜单管理', '#!menus', 'system/menus.html', null, 'layui-icon-menu-fill', '4', '2017-11-17 16:56:59', '2018-09-03 02:23:47', '1', '0');
INSERT INTO `sys_menu` VALUES ('9', '37', '文件中心', '#!files', 'files/files.html', null, 'layui-icon-file', '3', '2017-11-17 16:56:59', '2019-01-17 20:18:44', '1', '0');
INSERT INTO `sys_menu` VALUES ('10', '37', '文档中心', '#!swagger', '#/tpl/iframe/id=swagger', '', 'layui-icon-app', '4', '2017-11-17 16:56:59', '2019-09-17 18:45:26', '1', '0');
INSERT INTO `sys_menu` VALUES ('12', '-1', '认证管理', 'javascript:;', '', '', 'layui-icon-set', '99', '2017-11-17 16:56:59', '2019-09-01 20:15:43', '1', '0');
INSERT INTO `sys_menu` VALUES ('35', '12', '应用管理', '#!app', 'attestation/app.html', null, 'layui-icon-link', '5', '2017-11-17 16:56:59', '2019-01-14 15:35:15', '1', '0');
INSERT INTO `sys_menu` VALUES ('37', '-1', '系统管理', 'javascript:;', '', '', 'layui-icon-set', '98', '2018-08-25 10:41:58', '2019-09-01 20:16:03', '1', '0');
INSERT INTO `sys_menu` VALUES ('62', '63', '应用监控', '#!admin', 'http://127.0.0.1:6500/#/wallboard', null, 'layui-icon-chart-screen', '3', '2019-01-08 15:32:19', '2019-01-17 20:22:44', '1', '1');
INSERT INTO `sys_menu` VALUES ('63', '-1', '系统监控', 'javascript:;', '', null, 'layui-icon-set', '2', '2019-01-10 18:35:05', '2019-01-10 18:35:05', '1', '1');
INSERT INTO `sys_menu` VALUES ('64', '63', '系统日志', '#!sysLog', 'log/sysLog.html', null, 'layui-icon-file-b', '1', '2019-01-10 18:35:55', '2019-01-12 00:27:20', '1', '1');
INSERT INTO `sys_menu` VALUES ('65', '37', '代码生成器', '#!generator', 'generator/list.html', null, 'layui-icon-template', '2', '2019-01-14 00:47:36', '2019-01-23 14:06:31', '1', '0');
INSERT INTO `sys_menu` VALUES ('66', '63', '慢查询SQL', '#!slowQueryLog', 'log/slowQueryLog.html', null, 'layui-icon-snowflake', '2', '2019-01-16 12:00:27', '2019-01-16 15:32:31', '1', '1');
INSERT INTO `sys_menu` VALUES ('67', '-1', '任务管理', '#!job', 'http://127.0.0.1:8081/', null, 'layui-icon-date', '3', '2019-01-17 20:18:22', '2019-01-23 14:01:53', '1', '1');
INSERT INTO `sys_menu` VALUES ('68', '63', '应用吞吐量监控', '#!sentinel', 'http://127.0.0.1:6999', null, 'layui-icon-chart', '4', '2019-01-22 16:31:55', '2019-01-22 16:34:03', '1', '1');
INSERT INTO `sys_menu` VALUES ('69', '37', '配置中心', '#!nacos', 'http://127.0.0.1:8848/nacos', null, 'layui-icon-tabs', '1', '2019-01-23 14:06:10', '2019-01-23 14:06:10', '1', '1');
INSERT INTO `sys_menu` VALUES ('70', '63', 'APM监控', '#!apm', 'http://127.0.0.1:8080', null, 'layui-icon-engine', '5', '2019-02-27 10:31:55', '2019-02-27 10:31:55', '1', '1');
INSERT INTO `sys_menu` VALUES ('71', '-1', '搜索管理', 'javascript:;', '', null, 'layui-icon-set', '3', '2018-08-25 10:41:58', '2019-01-23 15:07:07', '1', '1');
INSERT INTO `sys_menu` VALUES ('72', '71', '索引管理', '#!index', 'search/index_manager.html', null, 'layui-icon-template', '1', '2019-01-10 18:35:55', '2019-01-12 00:27:20', '1', '1');
INSERT INTO `sys_menu` VALUES ('73', '71', '用户搜索', '#!userSearch', 'search/user_search.html', null, 'layui-icon-user', '2', '2019-01-10 18:35:55', '2019-01-12 00:27:20', '1', '1');
INSERT INTO `sys_menu` VALUES ('74', '12', 'Token管理', '#!tokens', 'system/tokens.html', null, 'layui-icon-unlink', '6', '2019-07-11 16:56:59', '2019-07-11 16:56:59', '1', '0');
INSERT INTO `sys_menu` VALUES ('75', '2', '用户列表', '/api-user/users', 'user-list', 'GET', null, '1', '2019-07-29 16:56:59', '2019-07-29 16:56:59', '2', '0');
INSERT INTO `sys_menu` VALUES ('76', '2', '查询用户角色', '/api-user/roles', 'user-roles', 'GET', null, '2', '2019-07-29 16:56:59', '2019-07-29 16:56:59', '2', '0');
INSERT INTO `sys_menu` VALUES ('77', '2', '用户添加', '/api-user/users/saveOrUpdate', 'user-btn-add', 'POST', null, '3', '2019-07-29 16:56:59', '2019-07-29 16:56:59', '2', '0');
INSERT INTO `sys_menu` VALUES ('78', '2', '用户导出', '/api-user/users/export', 'user-btn-export', 'POST', null, '4', '2019-07-29 16:56:59', '2019-07-29 16:56:59', '2', '0');
INSERT INTO `sys_menu` VALUES ('79', '2', '用户导入', '/api-user/users/import', 'user-btn-import', 'POST', null, '5', '2019-07-29 16:56:59', '2019-07-29 16:56:59', '2', '0');
INSERT INTO `sys_menu` VALUES ('85', '12', '字典管理', '#!dictionary', 'dictionary/dictionary.html', '', 'layui-icon-app', '7', '2019-08-19 18:17:35', '2019-08-19 18:32:07', '1', '0');
INSERT INTO `sys_menu` VALUES ('86', '12', '員工管理', '#!ersonnel', 'flats/ersonnel.html', '', 'layui-icon-app', '9', '2019-08-28 16:45:14', '2019-08-28 16:45:14', '1', '0');
INSERT INTO `sys_menu` VALUES ('90', '-1', '测试页面', '#!test', 'test/test.html', '', 'layui-icon-app', '0', '2019-09-10 17:35:36', '2019-09-10 17:35:47', '1', '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL COMMENT '角色code',
  `name` varchar(50) NOT NULL COMMENT '角色名',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'ADMIN', '管理员', '2017-11-17 16:56:59', '2018-09-19 09:39:10');
INSERT INTO `sys_role` VALUES ('2', 'test', '测试', '2018-09-17 10:15:51', '2018-11-15 01:49:14');
INSERT INTO `sys_role` VALUES ('5', '123', '12345', '2019-08-28 00:07:53', '2019-08-28 11:49:34');
INSERT INTO `sys_role` VALUES ('6', 'asd', 'asd', '2019-08-28 11:03:40', '2019-08-28 11:03:40');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '2');
INSERT INTO `sys_role_menu` VALUES ('1', '3');
INSERT INTO `sys_role_menu` VALUES ('1', '4');
INSERT INTO `sys_role_menu` VALUES ('1', '9');
INSERT INTO `sys_role_menu` VALUES ('1', '10');
INSERT INTO `sys_role_menu` VALUES ('1', '12');
INSERT INTO `sys_role_menu` VALUES ('1', '35');
INSERT INTO `sys_role_menu` VALUES ('1', '37');
INSERT INTO `sys_role_menu` VALUES ('1', '62');
INSERT INTO `sys_role_menu` VALUES ('1', '63');
INSERT INTO `sys_role_menu` VALUES ('1', '64');
INSERT INTO `sys_role_menu` VALUES ('1', '65');
INSERT INTO `sys_role_menu` VALUES ('1', '66');
INSERT INTO `sys_role_menu` VALUES ('1', '67');
INSERT INTO `sys_role_menu` VALUES ('1', '68');
INSERT INTO `sys_role_menu` VALUES ('1', '69');
INSERT INTO `sys_role_menu` VALUES ('1', '70');
INSERT INTO `sys_role_menu` VALUES ('1', '71');
INSERT INTO `sys_role_menu` VALUES ('1', '72');
INSERT INTO `sys_role_menu` VALUES ('1', '73');
INSERT INTO `sys_role_menu` VALUES ('1', '74');
INSERT INTO `sys_role_menu` VALUES ('1', '75');
INSERT INTO `sys_role_menu` VALUES ('1', '76');
INSERT INTO `sys_role_menu` VALUES ('1', '77');
INSERT INTO `sys_role_menu` VALUES ('1', '78');
INSERT INTO `sys_role_menu` VALUES ('1', '79');
INSERT INTO `sys_role_menu` VALUES ('1', '85');
INSERT INTO `sys_role_menu` VALUES ('1', '86');
INSERT INTO `sys_role_menu` VALUES ('1', '90');
INSERT INTO `sys_role_menu` VALUES ('2', '2');
INSERT INTO `sys_role_menu` VALUES ('2', '3');
INSERT INTO `sys_role_menu` VALUES ('2', '4');
INSERT INTO `sys_role_menu` VALUES ('2', '11');
INSERT INTO `sys_role_menu` VALUES ('2', '12');
INSERT INTO `sys_role_menu` VALUES ('2', '35');
INSERT INTO `sys_role_menu` VALUES ('6', '2');
INSERT INTO `sys_role_menu` VALUES ('6', '3');
INSERT INTO `sys_role_menu` VALUES ('6', '4');
INSERT INTO `sys_role_menu` VALUES ('6', '11');
INSERT INTO `sys_role_menu` VALUES ('6', '12');
INSERT INTO `sys_role_menu` VALUES ('6', '35');
INSERT INTO `sys_role_menu` VALUES ('6', '74');
INSERT INTO `sys_role_menu` VALUES ('6', '75');
INSERT INTO `sys_role_menu` VALUES ('6', '76');
INSERT INTO `sys_role_menu` VALUES ('6', '77');
INSERT INTO `sys_role_menu` VALUES ('6', '78');
INSERT INTO `sys_role_menu` VALUES ('6', '79');
INSERT INTO `sys_role_menu` VALUES ('6', '85');

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES ('1', '1');
INSERT INTO `sys_role_user` VALUES ('2', '1');
INSERT INTO `sys_role_user` VALUES ('3', '1');
INSERT INTO `sys_role_user` VALUES ('7', '2');
INSERT INTO `sys_role_user` VALUES ('8', '2');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `head_img_url` varchar(1024) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(16) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `open_id` varchar(32) DEFAULT NULL,
  `is_del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `username` (`username`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `open_id` (`open_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '$2a$10$TJkwVdlpbHKnV45.nBxbgeFHmQRmyWlshg94lFu2rKxVtT2OMniDO', 'Memories', '39194f38d8ad4d94a246c146e2c40812', '18888888888', '0', '1', 'APP', '2017-11-17 16:56:59', '2019-09-10 11:50:57', 'ENGJ', '123', '0');
INSERT INTO `sys_user` VALUES ('2', 'user', '$2a$10$OMgfjefyCKff6tDlHGlE4uT7jDVw1UY.ufn52QKcsRimxtwRVK5y.', '体验用户', 'http://payo7kq4i.bkt.clouddn.com/QQ%E5%9B%BE%E7%89%8720180819191900.jpg', '18888888887', '1', '0', 'APP', '2017-11-17 16:56:59', '2019-09-06 18:00:31', 'ENGJ', null, '0');
INSERT INTO `sys_user` VALUES ('3', 'test', '$2a$10$RD18sHNphJMmcuLuUX/Np.IV/7Ngbjd3Jtj3maFLpwaA6KaHVqPtq', '测试账户', 'http://payo7kq4i.bkt.clouddn.com/QQ%E5%9B%BE%E7%89%8720180819191900.jpg', '13851539156', '0', '0', 'APP', '2017-11-17 16:56:59', '2018-09-07 03:27:40', 'ENGJ', null, '0');
INSERT INTO `sys_user` VALUES ('4', '1', '$2a$10$9vLdwXBZaAPy/hmzEDf.M.YbrsKWGG21nqWq17/EwWPBi65GDivLa', '11', null, '13530151800', '1', '0', 'APP', '2018-09-07 14:20:51', '2019-08-27 15:40:33', 'YCC', null, '1');
INSERT INTO `sys_user` VALUES ('5', '12', '$2a$10$cgRGZ0uuIAoKuwBoTWmz7eJzP4RUEr688VlnpZ4BTCz2RZEt0jrIe', '12', null, '17587132062', '0', '0', 'APP', '2018-09-08 04:52:25', '2019-08-27 15:40:32', 'YCC', null, '1');
INSERT INTO `sys_user` VALUES ('6', 'abc1', '$2a$10$pzvn4TfBh2oFZJbtagovFe56ZTUlTaawPnx0Yz2PeqGex0xbddAGu', '1231321', null, '12345678901', '0', '0', 'APP', '2018-09-11 08:02:25', '2019-08-27 09:39:54', 'YCC', null, '1');
INSERT INTO `sys_user` VALUES ('7', '234', '$2a$10$FxFvGGSi2RCe4lm5V.G0Feq6szh5ArMz.8Mzm08zQlkA.VgE9GFbm', 'ddd', null, '13245678906', '0', '1', 'APP', '2018-09-19 01:33:54', '2018-09-19 01:33:54', 'JFSC', null, '1');
INSERT INTO `sys_user` VALUES ('8', 'tester', '$2a$10$VUfknatgKIoZJYDLIesrrO5Vg8Djw5ON2oDWeXyC24TZ6Ca/TWiye', 'tester', null, '12345678901', '0', '1', 'APP', '2018-09-19 04:52:01', '2018-11-16 22:12:04', 'JFSC', null, '1');
INSERT INTO `sys_user` VALUES ('9', '11111111111111111111', '$2a$10$DNaUDpCHKZI0V9w.R3wBaeD/gGOQDYjgC5fhju7bQLfIkqsZV61pi', 'cute', 'http://payo7kq4i.bkt.clouddn.com/C:\\Users\\GAOY91\\Pictures\\79f0f736afc37931a921fd59e3c4b74543a91170.jpg', '15599999991', '1', '1', 'APP', '2018-09-19 04:57:39', null, 'JFSC', null, '1');
INSERT INTO `sys_user` VALUES ('10', 'test001', '123456', 'test001', null, '11111111', '0', '1', 'BACKEND', '2018-09-12 13:50:57', '2019-01-07 13:04:18', null, null, '1');
INSERT INTO `sys_user` VALUES ('11', 'test002', '123456', 'test002', null, '22222222', '0', '1', 'BACKEND', '2018-09-11 08:02:25', '2018-09-14 06:49:54', null, null, '1');
INSERT INTO `sys_user` VALUES ('12', '123', '$2a$10$PgngbC9pQWDT.ZG37fvV6e8Zi0C3mQOVMJJE35.XQULnppSEWhyPK', '12', null, '1', '0', '1', 'BACKEND', '2019-01-19 13:44:02', '2019-01-19 13:44:02', null, null, '1');

-- ----------------------------
-- Table structure for t_personnel
-- ----------------------------
DROP TABLE IF EXISTS `t_personnel`;
CREATE TABLE `t_personnel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `simple_name` varchar(20) DEFAULT NULL COMMENT '简称',
  `big_name` varchar(50) DEFAULT NULL COMMENT '全名',
  `sex` char(2) DEFAULT NULL,
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `money` decimal(12,2) DEFAULT NULL COMMENT '金额',
  `eff_date` datetime DEFAULT NULL COMMENT '有效期',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='员工信息';

-- ----------------------------
-- Records of t_personnel
-- ----------------------------
INSERT INTO `t_personnel` VALUES ('1', '小明', '小明', '1', '12', '123.00', '2019-08-28 08:00:00', '2019-08-28 17:30:05', '2019-08-28 17:30:05');
INSERT INTO `t_personnel` VALUES ('3', 'sss', 'sss', '1', '12', '123.00', '2019-08-27 08:00:00', '2019-08-28 18:14:40', '2019-08-28 18:14:40');

-- ----------------------------
-- Table structure for t_tx_exception
-- ----------------------------
DROP TABLE IF EXISTS `t_tx_exception`;
CREATE TABLE `t_tx_exception` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` varchar(64) DEFAULT NULL,
  `unit_id` varchar(32) DEFAULT NULL,
  `mod_id` varchar(128) DEFAULT NULL,
  `transaction_state` tinyint(4) DEFAULT NULL,
  `registrar` tinyint(4) DEFAULT NULL,
  `remark` varchar(4096) DEFAULT NULL,
  `ex_state` tinyint(4) DEFAULT NULL COMMENT '0 未解决 1已解决',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_tx_exception
-- ----------------------------

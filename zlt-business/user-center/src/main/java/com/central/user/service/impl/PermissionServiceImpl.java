package com.central.user.service.impl;

import com.central.common.model.SysMenu;
import com.central.oauth2.common.service.impl.DefaultPermissionServiceImpl;
import com.central.user.controller.SysMenuController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 请求权限判断service
 *
 * @author zlt
 * @date 2018/10/28
 */
@Slf4j
@Service("permissionService")
public class PermissionServiceImpl extends DefaultPermissionServiceImpl {

    @Resource
    private SysMenuController menuController;

    @Override
    public List<SysMenu> findMenuByRoleCodes(String roleCodes) {
        return menuController.findMenuByRoles(roleCodes);
    }
}
